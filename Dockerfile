FROM schmich/armv7hf-alpine-qemu:3.5

RUN apk --update add socat
RUN apk add bash
COPY ./start /

CMD ["sh","/start"]
